Remote repository(somewhere in the internet)------------>
    git push                                            |
        |                                               |
Files to be sent to the remote repository               |
    git commit -m "message"                             |git pull(if repository exists)
        |                                               |
Staging area                                            |
    git add .                                           |
        |                                               |
Local computer  <---------------------------------------|


git clone repository_url : Take remote repository to your local machine at first time
git pull : Take the latest changes from the remote repository
git add file_name.py : add file to the staging area(second one)
git add . : add all files to the staging area
git commit -m "sensitive message" : Prepare files to be pushed to the remote repository(third area)
git push: sent all you changes from the third area(which is your commits) to the remote repository
