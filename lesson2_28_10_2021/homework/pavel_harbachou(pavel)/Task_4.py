input_value = input("Enter number : ")
try :
    number = int(input_value)
except ValueError :
    print("Only members are supported")
if number < 0:
    print('exact number is negative')
elif number >= 0:
    if number % 2 == 0:
        print("Even number")
    else :
        print("Odd number")
