'''
Create a car with color, model, current mileage, max mileage. By default max mileage=current mileage + 1000
Car should have 2 methods:
1.print_info: which will print all information about car e.g “This is color model car”
2. drive. Which accepts kms to drive. Method should print any message every km it has drive.
If it is more or equals max mileage then car is broken and should print it
'''

from task_5_class import Car

test_car = Car()

color = input('Enter your car color: ')
model = input('Enter your car model: ')
mileage = int(input('Enter your car mileage: '))

test_car.print_info(color, model, mileage)

km = int(input('\nHow many kms do you want to drive? '))
test_car.drive(km, mileage)
