# Two numbers are given. Find an average and geometrical mean
import statistics
a = 6
b = 10
print("The value of average numbers", a, "and", b, "is", statistics.mean([float(a), float(b)]))
print("The value of geometrical mean", a, "and", b, "is", round(statistics.geometric_mean([float(a), float(b)]), 2))
