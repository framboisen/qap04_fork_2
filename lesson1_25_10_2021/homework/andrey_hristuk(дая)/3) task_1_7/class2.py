# Модифицировать задачу 6 так:
# Если кол-то километров больше 5, то вывести сообщение :
# “Sorry. I can’t walk so much” Если кол-во километров 5 и меньше, то оставить сообщение прежнем.

class Human:
    def walk(self, kms):
        # Using argument 'if'
        if int(kms) <= 5:
            print('I went', kms, 'kms')
        else:
            print('Sorry. I can’t walk so much')
